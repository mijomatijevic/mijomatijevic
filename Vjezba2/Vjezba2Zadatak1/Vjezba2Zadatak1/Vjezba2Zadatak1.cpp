#include <iostream>
using namespace std;

void funkcija(int n[], int& min, int& max, int s)
{
    int i;
    for (i = 1; i < s; i++)
    {
        if ( min > n[i] )
            min = n[i];
        else if ( max < n[i] )
            max = n[i];
    }
}

int main()
{

    int n[6] = { 23, 13, 15, 98, 120, 56 };
    int s = sizeof(n) / sizeof(n[0]);
    int min = n[0];
    int max = n[0];
    funkcija( n, min, max, s );
    cout << "Minimum je: " << min << endl;
    cout << "maximum je: " << max << endl;

    return 0;
}