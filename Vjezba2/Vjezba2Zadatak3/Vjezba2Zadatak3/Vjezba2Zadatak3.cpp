#include <iostream>
using namespace std;

int& funkcija(int n[6], int i)
{
    return n[i];
}

int main()
{
    int indeks;
    int n[6] = { 1,2,3,4,5,6 };
    cout << "Unesite indeks: " << endl;
    cin >> indeks;
    funkcija(n, indeks) += 1;
    //cout<<n[indeks];
    cout << "Uvecan i-ti element niza za 1 iznosi:" << n[indeks] << endl;

    return 0;
}