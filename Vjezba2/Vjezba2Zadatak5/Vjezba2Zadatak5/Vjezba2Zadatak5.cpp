#include <iostream>

using namespace std;

int* duplo( int* arr, int size )
{
    int i = 0;
    int* temp = new int[size * 2];
    for ( i = 0; i < size; i++ )
    {
        temp[i] = arr[i];
    }
    delete[] arr;
    return temp;
}

int* funkcija( int& size, int& br )
{
    int i = 0;
    int* arr = new int[10];
    int broj = 1;

    cout << " Unesite broj, za prestanak unosa unesite 0 " << endl;

    while ( broj != 0 )
    {
        cin >> broj;
        if ( broj == 0 ) 
            break;

        arr[i] = broj;
        i++;
        br++;

        if ( i == size )
        {
            arr = duplo( arr, size );
            size = size * 2;
        }
    }
    return arr;
}

int main()
{
    int i = 0, br = 0, size = 10;
    int* arr = funkcija ( size, br );

    for ( i = 0; i < br; i++ )
    {
        cout << arr[i];
    }
 
    delete[] arr;

    return 0;
}