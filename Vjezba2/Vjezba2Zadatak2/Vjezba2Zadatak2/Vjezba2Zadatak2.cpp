#include <iostream>

using namespace std;

struct kruznica
{
	int x;
	int y;
	int radius;
};

bool funkcija(const kruznica& c1, const kruznica& c2)
{

	double d = (c1.x - c2.x) * (c1.x - c2.x) + (c1.y - c2.y) * (c1.y - c2.y);

	if (((c1.radius - c2.radius) * (c1.radius - c2.radius) <= d) && (d <= (c1.radius + c2.radius) * (c1.radius + c2.radius)))
		return true;
	else
		return false;

}

int main()
{
	kruznica circle1, circle2;

	circle1.x = 5;
	circle1.y = 5;
	circle1.radius = 5;

	circle2.x = 40;
	circle2.y = 30;
	circle2.radius = 6;

	bool r = funkcija(circle1, circle2);

	cout << boolalpha << r << endl;

	return 0;
}