#include <iostream>
#include <string>

using namespace std;

string funkcija(string s, string subs)
{	
	for(int i=0; i < s.size(); i++)
	{
		i = s.find(subs);
		if (i > 0 && i < s.size())
			s.erase(s.begin() + i, s.begin() + i + subs.size());
		if (i > s.size())
			break;
	}
	
	return s;
}

int main()
{
	string s = "anavolimilovanaanavolimilovanaanavolimilovana.";
	string subs = "voli";

	string novis = funkcija(s, subs);

	cout << novis;

	return 0;
}