#include <iostream> 
#include <stdlib.h> 
#include <string.h> 

using namespace std;

string funkcija(string s, string s1, int m, int n)
{
    int len[100][100];
    int l = 0, br;

    for (int i = 0; i <= m; i++)
    {
        for (int j = 0; j <= n; j++)
        {
            if (i == 0 || j == 0)
                len[i][j] = 0;

            else if (s[i - 1] == s1[j - 1])
            {
                len[i][j] = len[i - 1][j - 1] + 1;

                if (l < len[i][j])
                {
                    l = len[i][j];
                    br = i - 1;
                }
            }
            else
                len[i][j] = 0;
        }
    }

    return s.substr(br - l + 1, l);
}

int main()
{
    string str = "AnavoliMiju";
    string str1 = "MijovoliAnu";

    int m = str.size();
    int n = str1.size();

    string result = funkcija(str, str1, m, n);

    cout << result << endl;

    return 0;
}