#include <iostream>
#include <string>

using namespace std;

string funkcija(string s)
{
	reverse(s.begin(), s.end() - 1); //-1=bez tocke
		int j = 0;
	for (int i = 0; i < s.size(); i++)
	{
		if (s[i] == ' ' || s[i] == '.')
		{
			reverse(s.begin() + j, s.begin() + i);
			j = i+1;			
		}
	}
	s[0] = toupper(s[0]);
	for (int k = 1; k < s.size(); k++)
		s[k] = tolower(s[k]);

	return s;
}

int main()
{
	string s = "Ana voli Miju.";
	string novis = funkcija(s);

	cout << novis;

	return 0;
}