#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <random>

using namespace std;

string funkcija(string s)
{
	//cout << s;
	string temp = "";
	vector<string> nstr;

	for (int i = 0; i < s.size() + 1; i++)
	{
		if (s[i] == ' ' || i == s.size())
		{
			nstr.push_back(temp);
			temp.clear();
		}
		else
			temp = temp + s[i];
	}
	temp.clear();

	for (int j = 0; j < nstr.size(); j++)
	{
		if (nstr[j][0] == 'a' || nstr[j][0] == 'e' || nstr[j][0] == 'i' || nstr[j][0] == 'o' || nstr[j][0] == 'u')
		{
			nstr[j].push_back('h');
			nstr[j].push_back('a');
			nstr[j].push_back('y');
		}
		else
		{
			int br = 0;
			while (true)
			{
				if (nstr[j][br] == 'a' || nstr[j][br] == 'e' || nstr[j][br] == 'i' || nstr[j][br] == 'o' || nstr[j][br] == 'u')
					break;
				else
				{
					nstr[j].push_back(nstr[j][br]);
					nstr[j].erase(br, 1);
				}
			}
			nstr[j].push_back('a');
			nstr[j].push_back('y');
		}
	}
	for (int i = 0; i < nstr.size(); i++)
	{
		for (int j = 0; j < nstr.size(); j++)
			temp.push_back(nstr[i][j]);
		temp.push_back(' ');
	}
	return temp;
}

int main()
{
	int br = 0;
	string temp;
	vector<string> str;

	cout << " Unosi recenice, za kraj unesi kraj! " << endl;

	while (temp != "kraj")
	{
		getline(cin, temp);
		str.push_back(temp);
		br++; //broj recenica
		//cout << temp;
	}
	str.erase(str.begin() + br-1);

	string picked = str[rand() % br];

	cout << picked << endl;
	string rez = funkcija(picked);
	cout << rez << endl;

	return 0;
}