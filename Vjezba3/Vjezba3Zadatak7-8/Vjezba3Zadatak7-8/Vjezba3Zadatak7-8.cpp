#include <iostream>
#include <list>
#include <iterator>

using namespace std;

list<int> stuffing_bit(list<int> bit)
{
	int i = 0;
	list<int> ::iterator it;
	for (it = bit.begin(); it != bit.end(); it++)
	{
		if (*it == 1)
			i++;
		else
			i = 0;

		if (i == 5)
		{
			it++;
			bit.insert(it, 1, 0);
			i = 0;
		}
	}
	bit.push_back(0);
	bit.push_back(1111110);
	bit.push_front(1111110);
	bit.push_front(0);

	return bit;
}

list<int> unstuffing_bit(list<int> bit)
{
	int one_count = 0, zero_count = 0;
	bit.pop_front();
	bit.pop_back();
	list<int> ::iterator it;

	for (it = bit.begin(); it != bit.end(); it++)
	{
		if (*it == 1)
		{
			one_count++;
			zero_count = 0;
		}
		else
			zero_count++;

		if (one_count == 5 && zero_count == 1)
			it = bit.erase(it);
	}
	return bit;	
}

int main()
{
	int b;
	list<int> bit;

	cout << " Unosite bitove: " << endl;

	while (true)
	{
		cin >> b;
		if (b == 0 || b == 1)
			bit.push_back(b);
		else
			break;
	}
	
	list<int> rezultat_stuffing = stuffing_bit(bit);
	list<int>::iterator it;
	cout << "HDLC: ";
	for (it = rezultat_stuffing.begin(); it != rezultat_stuffing.end(); it++)
	{
		cout << *it;
	}
	cout << "\n";

	list<int> rezultat_unstuffing = unstuffing_bit(bit);
	list<int>::iterator it1;
	cout << "Uneseno: ";
	for (it1 = rezultat_unstuffing.begin(); it1 != rezultat_unstuffing.end(); it1++)
		cout << *it1;

	return 0;
}