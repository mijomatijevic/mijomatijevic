#include <iostream>
#include <vector>

using namespace std;

vector<int> funkcija(vector<int> v, int& min, int& max)
{
	vector<int> novi;
	int br = 1;
	int size = v.size() / 2;

	for (int i = 0; i < size; i++)
	{
		novi.push_back(v[i] + v[v.size() - br]);
		br++;
		
		if (novi[novi.size() - 1] > max)
			max = novi[novi.size() - 1];
		if (novi[novi.size() - 1] < min)
			min = novi[novi.size() - 1];
	}
	
	return novi;
}

int main()
{
	vector<int> v;
	int min, max;

	v.push_back(8);
	v.push_back(2);
	v.push_back(3);
	v.push_back(43);
	v.push_back(52);
	v.push_back(63);
	v.push_back(37);
	v.push_back(1);
	min = max = v[0];
	vector<int> sum = funkcija(v, min, max);
	
	for (int i = 0; i < sum.size(); i++)
	{
		cout << "|";
		cout << sum[i] << "|";
	}

	cout << " Max je: " << max << endl;
	cout << " Min je: " << min << endl;

	return 0;	
}