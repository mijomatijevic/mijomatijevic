#include <iostream>
#include <list>
#include <iterator>

using namespace std;

int funkcija(list<int>& lista, int& first, int& last, list<int>::iterator& it)
{
	int br = 0;

	for (it = lista.begin(); it != lista.end(); it++)
	{
		if (*it % 2 == 0)
		{
			*it++;
			lista.insert(it, 0);
		}
		if (*it % 2 != 0 && *it != 1)
		{
			*it++;
			lista.insert(it, 1);
		}
	}
	for (it = lista.begin(); it != lista.end(); it++)
	{
		br++;
		if (*it == 0)
	{
			first = br + 1;
			break;
		}
	}

	list <int> ::iterator it1;	
	for (it1 = lista.begin(); it1 != lista.end(); it1++)
	{
		br++;
		if (*it1 == 1)
		{
			last = br - 1;
		}
	}	
	return (last-first);
}

int main()
{
	list<int> lista;
	int first, last, br, temp;
	list<int> ::iterator it;

	cout << " Koliko brojeva zelis unit: " << endl;
	cin >> br;

	cout << "Unesi brojeve u listu: " << endl;
	for (int i = 1; i <= br; i++)
	{
		cin >> temp;
		lista.push_back(temp);
	}

	int rezultat = funkcija(lista, first, last, it);

	for (it = lista.begin(); it != lista.end(); it++)
		cout << *it << endl;
	cout << endl;
		
	cout << " Broj elemeata izmedu prve 0 i zadnje 1 je: " << rezultat << "." << endl;

	return 0;
}