#include <iostream>
#include "Header.h"

using namespace std;

int main()
{
	int flag = 1;
	char NewGame;
	int PlayerNumber;

Again:
	while (flag)
	{
		cout << "How many players want's to play Treseta? (2 or 4)" << endl;
		cin >> PlayerNumber;
		if (PlayerNumber == 2 || PlayerNumber == 4)
			flag--;
		else
			cout << "Error! Please try again." << endl << endl;
	}

	flag = 1;

	treseta(PlayerNumber);

	while (flag)
	{
		cout << endl;
		cout << "Do you want to play again? [ y / n ]" << endl;
		cin >> NewGame;
		cout << endl;
		if (NewGame == 'y')
			goto Again;
		else
		{
			cout << "Thank you for playing." << endl;
			return 0;
		}
	}
}
