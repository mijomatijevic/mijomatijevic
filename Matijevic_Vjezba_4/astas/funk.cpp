#include "Header.h"

void treseta(int PlayerNumber)
{
    if (PlayerNumber == 2)
    {
        Player PlayerArr[2];

        cout << "Enter players names:" << endl;
        for (int i = 0; i < 2; i++)
            cin >> PlayerArr[i].PlayerName;
        Deck deck;
        deck.shuffle();

        for (int i = 0; i < 2; i++)
        {
            PlayerArr[i].cards = deck.GetCards();
            PlayerArr[i].PointsFromHand(PlayerArr[i].cards);
        }

        for (int i = 0; i < 2; i++)
            cout << PlayerArr[i].PlayerName << " : " << PlayerArr[i].Points << " bodova. " << endl;

    }
    if (PlayerNumber == 4)
    {
        Player PlayerArr[4];

        cout << "Enter players names:" << endl;
        for (int i = 0; i < 4; i++)
            cin >> PlayerArr[i].PlayerName;
        Deck deck;
        deck.shuffle();

        for (int i = 0; i < 4; i++)
        {
            PlayerArr[i].cards = deck.GetCards();
            PlayerArr[i].PointsFromHand(PlayerArr[i].cards);
        }

        for (int i = 0; i < 4; i++)
            cout << PlayerArr[i].PlayerName << " : " << PlayerArr[i].Points << " bodova. " << endl;
    }
}

card::card(int number, char suit)
{
    this->Number = number;
    this->Suit = suit;
}

card Deck::GetCard()
{
    return Cards[CurrentCard++];
}

vector<card> Deck::GetCards()
{
    vector<card> hand;
    for (int i = 0; i < 10; i++)
    {
        hand.push_back(GetCard());
    }
    return hand;
}

Deck::Deck()
{
    int i = 0, j = 0, CardNumber = 1;
    char suit[] = { 'B','K','S','D' };
    for (i = 0; i < 4; i++)
    {
        for (j = 1; j <= 10; j++)
        {
            CardNumber = j;
            if (j > 7)
                CardNumber += 3;
            Cards.push_back(card(CardNumber, suit[i]));
        }
    }
}

void Deck::shuffle()
{
    CurrentCard = 0;
    for (int i = 0; i < Cards.size(); i++)
    {
        int j = (rand() + time(0)) % Cards.size();
        card temp = Cards[i];
        Cards[i] = Cards[j];
        Cards[j] = temp;
    }
}



void Player::PointsFromHand(vector<card> Hand)
{
    int PointsCounter = 0, m = 0, n = 0, k = 0;
    char suit[]{ 'D','S','B','K' };
    int arr[]{ 1,2,3 };
    int array[3][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0} };

    while (m != 4) {
        char temp = suit[n];

        for (int i = 0; i < 10; i++)
        {
            if (Hand[i].Number == 1 && Hand[i].Suit == temp)
                array[0][k] = 1;

            else if (Hand[i].Number == 2 && Hand[i].Suit == temp)
                array[1][k] = 1;

            else if (Hand[i].Number == 3 && Hand[i].Suit == temp)
                array[2][k] = 1;
        }
        m++;
        n++;
        k++;
    }

    for (int i = 0; i < 4; i++)
    {
        if (array[0][i] == 1 && array[1][i] == 1 && array[2][i] == 1)
            PointsCounter = PointsCounter + 3;
    }

    int counter = 0;

    for (int i = 0; i < 3; i++)
    {
        counter = 0;
        if (array[i][0] == 1)
            counter++;
        if (array[i][1] == 1)
            counter++;
        if (array[i][2] == 1)
            counter++;
        if (array[i][3] == 1)
            counter++;
        if (counter == 3)
            PointsCounter = PointsCounter + 3;
        if (counter == 4) 
            PointsCounter = PointsCounter + 4;

        Points = PointsCounter;
        
    }
}