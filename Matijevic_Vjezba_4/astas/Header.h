#pragma once
#include<vector>
#include <iostream>
#include <random>
#include <ctime>

using namespace std;

void treseta(int);

class card {

public:

    int Number;
    char Suit;
    card(int, char);
};


class Deck {

    vector<card> Cards;
    int CurrentCard = 0;
    inline card GetCard();

public:
    Deck();
    void shuffle();
    vector<card> GetCards();
};

class Player {

public:
    string PlayerName;
    int Points = 0;
    vector<card> cards;
    void PointsFromHand(vector<card>);
};




